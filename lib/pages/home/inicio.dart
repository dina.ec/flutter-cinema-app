import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:io' show Platform;
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:starcines/models/movie.dart';
import 'package:starcines/pages/moviePage.dart';
import 'package:starcines/models/genre.dart';
import 'package:starcines/models/tvshow.dart';
import 'package:starcines/pages/tvPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:starcines/components/ImageUrlButton.dart';

import 'package:starcines/config.dart' as mconfig;

final String themoviedb = mconfig.themoviedb;
final Color pcolor = Color(0xFFff1744);

class Inicio extends StatefulWidget {
  BuildContext scaffoldContext;
  ScrollController scrollController;
  final VoidCallback onClick1, onClick2, onClick3, onClick4;

  Inicio({
    Key key,
    @required this.scaffoldContext,
    @required this.scrollController,
    this.onClick1,
    this.onClick2,
    this.onClick3,
    this.onClick4,
  }) : super(key: key);

  @override
  _InicioState createState() => new _InicioState(
      scaffoldContext: scaffoldContext,
      scrollController: scrollController,
      onClick1: onClick1,
      onClick2: onClick2,
      onClick3: onClick3,
      onClick4: onClick4);
}

class _InicioState extends State<Inicio> {
  final VoidCallback onClick1, onClick2, onClick3, onClick4;

  var isTop = true;
  ScrollController scrollController;
  final BuildContext scaffoldContext;
  List<Movie> movies = <Movie>[];
  List<TVShow> tvShows = <TVShow>[];
  List<TVShow> airTvShows = <TVShow>[];
  Movie movie;
  List<Movie> upcomingMovies = <Movie>[];

  var mwidth = 0.0;
  var mheight = 0.0;

  _InicioState({
    @required this.scaffoldContext,
    @required this.scrollController,
    this.onClick1,
    this.onClick2,
    this.onClick3,
    this.onClick4,
  });

  @override
  void initState() {
    super.initState();

    fetchMovies();
    fetchUpcomingMovies();
    fetchTopTV();
    fetchAirTV();
  }

  //metodo para leer las peliculas y convertirlas a un array de movies
  fetchMovies() async {
    final DateTime today = new DateTime.now();
    final DateTime lastDay = today.add(Duration(days: 5));
    List<Movie> tmpMovies = <Movie>[];
    var url =
        'https://api.themoviedb.org/3/movie/now_playing?api_key=$themoviedb&language=es-EC&page=1';

    print(url);

    final response = await http.get(url);
    var parsed = JSON.decode(response.body);
    for (var movieJSON in parsed['results']) {
      tmpMovies.add(Movie.fromJson(movieJSON));
    }
    var movieTmp = tmpMovies[0];
    tmpMovies.removeAt(0);
    setState(() {
      movie = movieTmp;
      movies = tmpMovies;
    });
  }

  fetchUpcomingMovies() async {
    List<Movie> tmpMovies = <Movie>[];
    final response = await http.get(
        "https://api.themoviedb.org/3/movie/upcoming?api_key=${themoviedb}&language=es-EC&page=1&region=US");
    var parsed = JSON.decode(response.body);

    for (var movieJSON in parsed['results']) {
      var movie = Movie.fromJson(movieJSON);
      if (movie.backdrop_path != null) {
        tmpMovies.add(Movie.fromJson(movieJSON));
      }
    }
    setState(() {
      upcomingMovies = tmpMovies;
    });
  }

  fetchTopTV() async {
    List<TVShow> tmp = <TVShow>[];
    var url =
        'https://api.themoviedb.org/3/tv/top_rated?api_key=$themoviedb&language=es&page=1';

    final response = await http.get(url);
    var parsed = JSON.decode(response.body);
    for (var item in parsed['results']) {
      tmp.add(TVShow.fromJSON(item));
    }
    setState(() {
      tvShows = tmp;
    });
  }

  fetchAirTV() async {
    List<TVShow> tmp = <TVShow>[];
    var url =
        'https://api.themoviedb.org/3/tv/on_the_air?api_key=$themoviedb&language=es&page=1';

    final response = await http.get(url);
    var parsed = JSON.decode(response.body);
    for (var item in parsed['results']) {
      tmp.add(TVShow.fromJSON(item));
    }
    setState(() {
      airTvShows = tmp;
    });
  }

  @override
  Widget build(BuildContext context) {
    mwidth = MediaQuery.of(context).size.width;
    mheight = MediaQuery.of(context).size.height;

    Widget _buildHeaderImage() {
      return movie != null
          ? Stack(
              children: <Widget>[
                new Image.network(
                  "${mconfig.themoviedb_img + movie.backdrop_path}",
                  fit: BoxFit.cover,
                  height: 400.0,
                  width: mwidth,
                  colorBlendMode: BlendMode.srcOver,
                  color: new Color.fromRGBO(0, 0, 0, 0.3),
                ),
                new Positioned(
                    bottom: 0.0,
                    left: 0.0,
                    right: 0.0,
                    child: new Container(
                      height: 180.0,
                      decoration: new BoxDecoration(
                        gradient: new LinearGradient(
                          // new
                          // Where the linear gradient begins and ends
                          begin: Alignment.bottomCenter, // new
                          end: Alignment.topCenter, // new
                          // Add one stop for each color.
                          // Stops should increase
                          // from 0 to 1
                          stops: [0.1, 0.5, 0.7, 0.9],
                          colors: [
                            // Colors are easy thanks to Flutter's
                            // Colors class.
                            Color.fromRGBO(28, 36, 41, 1.0),
                            Color.fromRGBO(28, 36, 41, 0.6),
                            Color.fromRGBO(28, 36, 41, 0.3),
                            Color.fromRGBO(28, 36, 41, 0.0),
                          ],
                        ),
                      ),
                    )),
                new Positioned(
                    left: 0.0,
                    right: 0.0,
                    bottom: 10.0,
                    child: Padding(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          new Expanded(
                              flex: 5,
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Material(
                                    child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 12.0, vertical: 5.0),
                                        child: Text(
                                          "ESTRENO",
                                          style: TextStyle(
                                              fontFamily: 'Anton',
                                              color: Colors.white,
                                              fontSize: 17.0,
                                              letterSpacing: 2.0),
                                        )),
                                    color: Colors.amber,
                                  ),
                                  SizedBox(height: 5.0),
                                  new Text(
                                    movie.title.toUpperCase(),
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontFamily: 'Anton',
                                        color: Colors.white,
                                        fontSize: 22.0,
                                        letterSpacing: 2.0),
                                  ),
                                  new Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.people,
                                        color: Colors.white,
                                        size: 30.0,
                                      ),
                                      SizedBox(width: 5.0),
                                      Text(
                                        "${movie.vote_count}",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      SizedBox(width: 20.0),
                                      Icon(
                                        Icons.star,
                                        color: Colors.white,
                                        size: 30.0,
                                      ),
                                      SizedBox(width: 5.0),
                                      Text(
                                        "${movie.vote_average} / 10",
                                        style: TextStyle(color: Colors.white),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 5.0),
                                  new Text(
                                    movie.overview,
                                    maxLines: 3,
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 13.0,
                                        height: 0.8),
                                  )
                                ],
                              )),
                          SizedBox(width: 10.0),
                          new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new CupertinoButton(
                                  padding: EdgeInsets.all(0.0),
                                  child: new Image.asset(
                                    'images/share.png',
                                    width: 30.0,
                                  ),
                                  onPressed: () {}),
                              SizedBox(height: 10.0),
                              new CupertinoButton(
                                  padding: EdgeInsets.all(0.0),
                                  child: new Image.asset(
                                    'images/heart.png',
                                    width: 30.0,
                                  ),
                                  onPressed: () {}),
                            ],
                          )
                        ],
                      ),
                    )),
                new Positioned(
                  top: 105.0,
                  left: 0.0,
                  right: 0.0,
                  child: new Center(
                    child: new CupertinoButton(
                      pressedOpacity: 0.3,
                      onPressed: () {
                        toMoviePage(scaffoldContext, movie);
                      },
                      child: new Container(
                        padding: EdgeInsets.only(
                            left: 25.0, top: 20.0, bottom: 20.0, right: 20.0),
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromRGBO(0, 0, 0, 0.5)),
                        child: Image.asset(
                          'images/playbutton.png',
                          width: 50.0,
                          height: 50.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          : Center(
              child: CircularProgressIndicator(),
            );
    }

    Widget _buildFab() {
      return new Positioned(
        top: 300.0 - 36.0,
        right: 16.0,
        child: new FloatingActionButton(
          heroTag: "PlayPromo",
          onPressed: () {},
          backgroundColor: Colors.amber,
          child: new Icon(
            Icons.play_arrow,
            size: 40.0,
          ),
        ),
      );
    }

    Widget _buildMovies() {
      return new SliverPadding(
        padding: EdgeInsets.only(left: 3.0, top: 4.0, right: 3.0),
        sliver: SliverToBoxAdapter(
            child: Column(
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text(
                  "EN CARTELERA",
                  style: TextStyle(
                      fontFamily: 'Anton',
                      letterSpacing: 2.0,
                      color: Colors.white),
                ),
                new GestureDetector(
                    onTap: () {
                      onClick1();
                    },
                    child: Row(
                      children: <Widget>[
                        Text(
                          "TODO",
                          style: TextStyle(color: pcolor),
                        ),
                        Icon(
                          Icons.arrow_drop_down,
                          color: pcolor,
                        )
                      ],
                    ))
              ],
            ),
            new Container(
              child: new ListView(
                scrollDirection: Axis.horizontal,
                children: movies.map((movie) {
                  return new ImageUrlButton(
                    url: "${mconfig.themoviedb_img}${movie.poster_path}",
                    onClick: () {
                      toMoviePage(scaffoldContext, movie);
                    },
                  );
                }).toList(),
              ),
              height: 175.0,
            )
          ],
        )),
      );
    }

    //peliculas proximas a estrenarse
    Widget _buildUpcomingMovies() {
      return new SliverPadding(
        padding: EdgeInsets.only(left: 3.0, top: 4.0, right: 3.0),
        sliver: SliverToBoxAdapter(
            child: Column(
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text(
                  "PROXIMOS ESTRENOS",
                  style: TextStyle(
                      fontFamily: 'Anton',
                      letterSpacing: 2.0,
                      color: Colors.white),
                ),
                new GestureDetector(
                    onTap: () {onClick2();},
                    child: Row(
                      children: <Widget>[
                        Text(
                          "TODO",
                          style: TextStyle(color: pcolor),
                        ),
                        Icon(
                          Icons.arrow_drop_down,
                          color: pcolor,
                        )
                      ],
                    ))
              ],
            ),
            new Container(
              child: new ListView(
                scrollDirection: Axis.horizontal,
                children: upcomingMovies.map((movie) {
                  return new ImageUrlButton(
                    url: "${mconfig.themoviedb_img}${movie.poster_path}",
                    onClick: () {
                      toMoviePage(scaffoldContext, movie);
                    },
                  );
                }).toList(),
              ),
              height: 175.0,
            )
          ],
        )),
      );
    }

    Widget _buildTopTV() {
      return new SliverPadding(
        padding: EdgeInsets.only(left: 3.0, top: 13.0, right: 3.0),
        sliver: SliverToBoxAdapter(
            child: Column(
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text(
                  "MEJORES SERIES DE TV",
                  style: TextStyle(
                      fontFamily: 'Anton',
                      letterSpacing: 2.0,
                      color: Colors.white),
                ),
                new GestureDetector(
                    onTap: () {},
                    child: Row(
                      children: <Widget>[
                        Text(
                          "TODO",
                          style: TextStyle(color: pcolor),
                        ),
                        Icon(
                          Icons.arrow_drop_down,
                          color: pcolor,
                        )
                      ],
                    ))
              ],
            ),
            new Container(
              child: new ListView(
                scrollDirection: Axis.horizontal,
                children: tvShows.map((tv) {
                  return new ImageUrlButton(
                      url: mconfig.themoviedb_img + tv.poster_path,
                      onClick: () {});
                }).toList(),
              ),
              height: 175.0,
            )
          ],
        )),
      );
    }

    Widget _buildNowTV() {
      return new SliverPadding(
        padding:
            EdgeInsets.only(left: 3.0, top: 13.0, bottom: 10.0, right: 3.0),
        sliver: SliverToBoxAdapter(
            child: Column(
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text(
                  "SERIES DE TV AL AIRE",
                  style: TextStyle(
                      fontFamily: 'Anton',
                      letterSpacing: 2.0,
                      color: Colors.white),
                ),
                new GestureDetector(
                    onTap: () {},
                    child: Row(
                      children: <Widget>[
                        Text(
                          "TODO",
                          style: TextStyle(color: pcolor),
                        ),
                        Icon(
                          Icons.arrow_drop_down,
                          color: pcolor,
                        )
                      ],
                    ))
              ],
            ),
            new Container(
              child: new ListView(
                scrollDirection: Axis.horizontal,
                children: airTvShows.map((tv) {
                  return new ImageUrlButton(
                      url: '${mconfig.themoviedb_img + tv.poster_path}',
                      onClick: () {});
                }).toList(),
              ),
              height: 175.0,
            )
          ],
        )),
      );
    }

    return movie != null
        ? new CustomScrollView(
            slivers: <Widget>[
              new SliverPadding(
                padding: EdgeInsets.all(0.0),
                sliver: SliverToBoxAdapter(
                    child: Stack(
                  children: <Widget>[
                    _buildHeaderImage(),
                  ],
                )),
              ),
              _buildMovies(),
              _buildUpcomingMovies(),
              _buildTopTV(),
              _buildNowTV(),
              SliverToBoxAdapter(
                child: SizedBox(height: 45.0),
              )
            ],
            controller: scrollController,
          )
        : Center(
            child: SpinKitDualRing(
              color: Colors.white,
              size: 80.0,
            ),
          );
  }
}
