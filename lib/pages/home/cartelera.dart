import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:starcines/models/movie.dart';
import 'package:flutter/cupertino.dart';
import 'package:starcines/components/ImageUrlButton.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:starcines/config.dart' as mconfig;

final String themoviedb = mconfig.themoviedb;
final Color pcolor = Color(0xFFff1744);

class Cartelera extends StatefulWidget {
  BuildContext scaffoldContext;
  ScrollController scrollController;

  Cartelera(
      {Key key,
      @required this.scaffoldContext,
      @required this.scrollController})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => new _CarteleraState(
      scaffoldContext: scaffoldContext, scrollController: scrollController);
}

class _CarteleraState extends State<Cartelera> {
  var isTop = true;
  ScrollController scrollController;
  final BuildContext scaffoldContext;

  List<Movie> movies = <Movie>[];

  var page = 1;
  var total_pages = 1;
  var loading = false;

  var mwidth = 0.0;
  var mheight = 0.0;

  List<String> promos = <String>[
    'https://scontent.fuio1-1.fna.fbcdn.net/v/t1.0-9/36700077_1740501272697394_2353475622455476224_o.jpg?_nc_cat=0&oh=0a2cd366e5301d304ac65ca176e0e79b&oe=5BEBED62',
    'https://scontent.fuio1-1.fna.fbcdn.net/v/t31.0-8/30167867_1659044617509727_3191245837370878076_o.jpg?_nc_cat=0&oh=4c6d1275272e4ee746b336c93c1c1878&oe=5BEA6A75'
  ];
  PageController controllerPromos = new PageController();
  int currentPagePromos = 0;

  _CarteleraState(
      {@required this.scaffoldContext, @required this.scrollController});

  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      print("scroll listener ${scrollController.position.extentAfter}");
      if (scrollController.position.extentAfter == 0 && loading == false) {
        if (page < total_pages) {
          page++;
          fetchMovies();
        }
      }
    });
    fetchMovies();
  }

  //metodo para leer las peliculas y convertirlas a un array de movies
  fetchMovies() async {
    setState(() {
      loading = true;
    });

    List<Movie> tmpMovies = movies;
    var url =
        'https://api.themoviedb.org/3/movie/now_playing?api_key=$themoviedb&language=es-EC&page=$page';

    final response = await http.get(url);
    var parsed = JSON.decode(response.body);
    for (var movieJSON in parsed['results']) {
      var movie = Movie.fromJson(movieJSON);
      if (movie.backdrop_path != null) {
        tmpMovies.add(movie);
      }
    }
    total_pages = parsed['total_pages'];
    setState(() {
      movies = tmpMovies;
      loading = false;
    });
  }

  Widget _buildMovies() {
    return new SliverPadding(
      padding: EdgeInsets.only(left: 3.0, top: 4.0, right: 3.0),
      sliver: new SliverGrid(
        gridDelegate: new SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: mwidth / 2 - 10,
          mainAxisSpacing: 2.0,
          crossAxisSpacing: 2.0,
          childAspectRatio: 2/3,
        ),
        delegate: new SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            var movie = movies[index];
            return new ImageUrlButton(
              url: "${mconfig.themoviedb_img}${movie.poster_path}",
              onClick: () {
                toMoviePage(scaffoldContext, movie);
              },
              width: mwidth / 2 - 10,
              height: 220.0,
            );
          },
          childCount: movies.length,
        ),
      ),
    );
  }

  _buildPromos() {
    return new SliverToBoxAdapter(
      child: Container(
        height: mwidth/3,
        child: new PageView.builder(
            controller: controllerPromos,
            itemCount: promos.length,
            itemBuilder: (context, index) => _builderPromos(index)),
      ),
    );
  }

  _builderPromos(int index) {
    return new AnimatedBuilder(
        animation: controllerPromos,
        builder: (context, child) {
          return new Center(
            child: new SizedBox(
              height: mwidth/3,
              width: mwidth,
              child: child,
            ),
          );
        },
        child: new Card(
          elevation: 3.0,
          child: ConstrainedBox(
            constraints: const BoxConstraints.expand(),
            child: new CachedNetworkImage(
              imageUrl: promos[index],
              placeholder: new Center(
                child: new CircularProgressIndicator(),
              ),
              errorWidget: new Icon(Icons.error),
              fit: BoxFit.cover,
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    mwidth = MediaQuery.of(context).size.width;
    mheight = MediaQuery.of(context).size.height;

    return movies.length > 0
        ? new CustomScrollView(
            slivers: <Widget>[
              SliverToBoxAdapter(
                child: SizedBox(height: 80.0),
              ),
              _buildPromos(),
              _buildMovies(),
              SliverToBoxAdapter(
                child: SizedBox(height: 45.0),
              )
            ],
            controller: scrollController,
          )
        : Center(
            child: SpinKitDualRing(
              color: Colors.white,
              size: 80.0,
            ),
          );
  }
}
