import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';

import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:starcines/config.dart' as mconfig;
//models
import 'package:starcines/models/movie_detail.dart';
import 'package:flutter/rendering.dart';

final Color pcolor = Color(0xFFff1744);
final String themoviedb = "20ac754dbe6ba584a7a8a8690c755249";

class Entrada {
  final int num;
  final String hora, dimension, idioma;
  final bool disponible;

  Entrada(this.num, this.hora, this.dimension, this.idioma, this.disponible);

  @override
  DataRow getRow() {
    return new DataRow(cells: [
      new DataCell(Text(num.toString())),
      new DataCell(Text(hora)),
      new DataCell(Text(dimension)),
      new DataCell(Text(idioma)),
      new DataCell(Text(dimension == true ? 'COMPRAR' : 'AGOTADO')),
    ]);
  }
}



class MoviePage extends StatefulWidget {
  final int movieId;

  MoviePage({Key key, @required this.movieId}) : super(key: key);

  @override
  _MoviePageState createState() => new _MoviePageState(movieId: movieId);
}

class _MoviePageState extends State<MoviePage> {
  var youtube = new FlutterYoutube();
  var mwidth = 0.0;
  var mheight = 0.0;
  MovieDetail movie;
  final int movieId;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final List<Entrada> entradas = <Entrada>[
    new Entrada(1, '11H00', '2D', 'Español', true),
    new Entrada(2, '13H030', '2D', 'Español', true),
    new Entrada(3, '15H00', '3D', 'Subtitulado', false),
  ];
  List<Casting> actores = List<Casting>();
  List<String> imgs = <String>[];
  List<String> reviews = <String>[];
  var trailer = ""; //trailer de la pelicula
  var runtimeH = ""; //duracion de la pelicula
  var runtimeM = ""; //duracion de la pelicula
  var genres = ""; //generos de la pelicula
  var tab = 0; //generos de la pelicula

  var showImg = true;

  _MoviePageState({@required this.movieId});

  BuildContext _context;

  @override
  void initState() {
    super.initState();
    fetchMovie();
  }

  /**
   * recupera los datos de la pelicula sgun el id
   */
  fetchMovie() async {
    var url =
        'https://api.themoviedb.org/3/movie/$movieId?api_key=$themoviedb&language=es-EC';
    final response = await http.get(url);
    var parsed = JSON.decode(response.body);
    if (parsed['id'] != null) {
      var tmp = MovieDetail.fromJson(parsed);
      //convertimos runtime a horas y minutos
      if (tmp.runtime != null) {
        setState(() {
          runtimeH = "${(tmp.runtime / 60).floor()}";
          runtimeM = "${tmp.runtime % 60}";
        });
      }
      var index = 0;
      for (var genre in tmp.genres) {
        genres +=
            "${genre['name']}${index < tmp.genres.length - 1 ? ',' : ''} ";
        index++;
      }

      setState(() {
        movie = tmp;
      });

      fetchCast(); //recuperamos el cast de la pelicula
      fetchImages(); //recuperamos las imagenes de la pelicula
      fetchTrailer(); //recuperamos el trailer de la pelicula

    }
  }

  //metodo para leer las peliculas y convertirlas a un array de movies
  fetchCast() async {
    List<Casting> tmp = <Casting>[];
    var url = 'https://api.themoviedb.org/3/movie/${movie
        .id}/credits?api_key=$themoviedb';

    final response = await http.get(url);
    var parsed = JSON.decode(response.body);
    for (var item in parsed['cast']) {
      if (Casting.fromJson(item).profile_path != null) {
        tmp.add(Casting.fromJson(item));
      }
    }

    setState(() {
      actores = tmp;
    });
  }

  fetchImages() async {
    List<String> tmp = <String>[];
    final response = await http.get("https://api.themoviedb.org/3/movie/${movie
        .id}/images?api_key=$themoviedb");
    var parsed = JSON.decode(response.body);
    var i = 0;
    for (var img in parsed['backdrops']) {
      if (i < 7) {
        tmp.add(img['file_path']);
        i++;
      } else {
        break;
      }
    }

    if (tmp.length > 1) {
      tmp.removeAt(0);
    }
    setState(() {
      imgs = tmp;
    });
  }

  fetchTrailer() async {
    final response = await http.get("https://api.themoviedb.org/3/movie/${movie
        .id}/videos?api_key=$themoviedb&language=es");
    var parsed = JSON.decode(response.body);
    var tmp = "";
    for (var img in parsed['results']) {
      tmp = img['key'];
      break;
    }
    setState(() {
      trailer = tmp;
    });
  }

  fetchReviews() async {
    final response = await http.get("https://api.themoviedb.org/3/movie/${movie
        .id}/reviews?api_key=$themoviedb&page=1");
    var parsed = JSON.decode(response.body);

    List<String> tmp = <String>[];
    for (var item in parsed['results']) {
      tmp.add(item['content']);
    }
    setState(() {
      reviews = tmp;
    });
  }

  List<Widget> _buildCategoryChips() {
    return ["Accion", "Comedia"].map((category) {
      return Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: Chip(
          label: Text(
            category,
            style: TextStyle(fontSize: 10.0),
          ),
          backgroundColor: Colors.black12,
        ),
      );
    }).toList();
  }

  Widget MovieDetailHeader() {
    var screenWidth = MediaQuery.of(_context).size.width;
    return SliverToBoxAdapter(
      child: Stack(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.only(bottom: 30.0),
            child: ClipPath(
              clipper: ArcClipper(),
              child: Image.network(
                "https://image.tmdb.org/t/p/w500/${movie
                    .backdrop_path}",
                width: screenWidth,
                height: 290.0,
                fit: BoxFit.cover,
                colorBlendMode: BlendMode.srcOver,
                color: new Color.fromARGB(120, 20, 10, 40),
              ),
            ),
          ),
          new Positioned(
            left: 20.0,
            right: 5.0,
            bottom: 10.0,
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Material(
                  borderRadius: BorderRadius.circular(4.0),
                  elevation: 5.0,
                  child: new CachedNetworkImage(
                    imageUrl: "https://image.tmdb.org/t/p/w500/${movie
                        .poster_path}",
                    width: 150.0,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        "IMDb",
                        style: TextStyle(
                            fontSize: 22.0,
                            fontFamily: 'Anton',
                            color: Colors.white),
                      ),
                      new Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          SizedBox(width: 5.0),
                          Text(
                            movie.vote_average.toString(),
                            style:
                                TextStyle(fontSize: 24.0, color: Colors.white),
                          ),
                          Text(
                            "/10",
                            style: TextStyle(
                                fontSize: 24.0, color: Color(0xFFd5d9d9d9)),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          new Positioned(
              top: 25.0,
              left: 10.0,
              right: 10.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back, color: Colors.white),
                    onPressed: () {
                      Navigator.pop(_context);
                    },
                  ),
                  Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.bookmark_border, color: Colors.white),
                        onPressed: () {
                          Navigator.pop(_context);
                        },
                      ),
                      IconButton(
                        icon: Icon(Icons.favorite_border, color: Colors.white),
                        onPressed: () {
                          Navigator.pop(_context);
                        },
                      ),
                    ],
                  )
                ],
              )),
          new Positioned(
            bottom: -7.0,
            right: 0.0,
            child: CupertinoButton(
              padding: EdgeInsets.all(0.0),
              pressedOpacity: 0.3,
              onPressed: () {
                youtube.playYoutubeVideoById(
                    apiKey: "AIzaSyBCfP7rIsyrywYhfuRJ4KUIDweeH7mbkSI",
                    videoId: trailer,
                    fullScreen: true //default false
                    );
              },
              child: Stack(
                children: <Widget>[
                  ClipPath(
                    clipper: TriClipper(),
                    child: Container(
                      width: screenWidth,
                      height: 290 / 2 + 15,
                      color: Colors.amber,
                    ),
                  ),
                  Positioned(
                    top: 290 / 8,
                    right: screenWidth / 12,
                    child: Center(
                        child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 50.0,
                    )),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSipnosis() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
      child: Column(
        children: <Widget>[
          Text(
            "Sipnosis:",
            textAlign: TextAlign.start,
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
          SizedBox(height: 5.0),
          Text(
            movie.overview,
            textAlign: TextAlign.justify,
            style: TextStyle(color: Color(0xFFffffff),height: 0.7),
          )
        ],
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }

  Widget _builCast() {
    return new Column(
      children: <Widget>[
        Padding(
          child: Text(
            "Cast",
            textAlign: TextAlign.start,
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
          padding: EdgeInsets.only(left: 20.0),
        ),
        SizedBox(height: 5.0),
        Container(
          child: new ListView(
            scrollDirection: Axis.horizontal,
            children: actores.map((cast) {
              return new Padding(
                padding: EdgeInsets.only(left: 15.0, bottom: 2.0),
                child: Column(
                  children: <Widget>[
                    Material(
                      borderRadius: BorderRadius.circular(50.0),
                      child: new CachedNetworkImage(
                        imageUrl: "https://image.tmdb.org/t/p/w500/${cast
                            .profile_path}",
                        placeholder: Container(
                          child: new Center(
                            child: new CircularProgressIndicator(),
                          ),
                          width: 100.0,
                          height: 100.0,
                        ),
                        errorWidget: Container(
                          child: new Icon(Icons.error),
                          width: 100.0,
                          height: 100.0,
                        ),
                        fit: BoxFit.cover,
                        height: 100.0,
                        width: 100.0,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 4.0),
                      child: Text(
                        cast.name,
                        style: TextStyle(fontSize: 11.0, color: Colors.white),
                        maxLines: 1,
                      ),
                    )
                  ],
                ),
              );
            }).toList(),
          ),
          height: 123.0,
        )
      ],
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }

  Widget _buildHead2() {
    return SliverToBoxAdapter(
      child: Padding(
        padding: EdgeInsets.only(left: 20.0, top: 15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
                flex: 3,
                child: new Padding(
                  padding: EdgeInsets.only(right: 10.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        movie.title,
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.w800,
                            color: Colors.white),
                        maxLines: 1,
                        softWrap: true,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "${runtimeH}H",
                                style: TextStyle(
                                    fontSize: 26.0,
                                    color: Color(0xFFffa328),
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "${runtimeM}min",
                                style: TextStyle(
                                    fontSize: 13.0,
                                    color: Color(0xFFffa328),
                                    height: 0.6),
                              )
                            ],
                          ),
                          SizedBox(width: 10.0),
                          Expanded(
                              child: Text(
                            genres,
                            style: TextStyle(
                                color: Colors.blueGrey, fontSize: 16.0),
                            softWrap: true,
                            maxLines: 2,
                          ))
                        ],
                      )
                    ],
                  ),
                )),
            Expanded(
              child: new CupertinoButton(
                padding: EdgeInsets.all(0.0),
                pressedOpacity: 0.2,
                child: Image.asset(
                  'images/ticket.png',
                  width: 200.0,
                ),
                onPressed: () {
                  setState(() {
                    tab = 2;
                  });
                },
              ),
              flex: 1,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTabs() {
    return new SliverToBoxAdapter(
      child: Padding(
        padding: EdgeInsets.only(top: 25.0),
        child: Material(
          elevation: 5.0,
          color: Color(0xFF263238),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              new Expanded(
                child: FlatButton(
                  child: Text(
                    "INFO",
                    style: TextStyle(color: tab == 0 ? pcolor : Colors.white,fontFamily: 'Anton', letterSpacing: 2.0),
                  ),
                  onPressed: () {
                    setState(() {
                      tab = 0;
                    });
                  },
                  padding: EdgeInsets.all(15.0),
                ),
              ),
              new Expanded(
                child: FlatButton(
                  child: Text(
                    "IMAGENES",
                    style: TextStyle(color: tab == 1 ? pcolor : Colors.white,fontFamily: 'Anton', letterSpacing: 2.0),
                  ),
                  onPressed: () {
                    setState(() {
                      tab = 1;
                    });
                  },
                  padding: EdgeInsets.all(15.0),
                ),
              ),
              new Expanded(
                child: FlatButton(
                  child: Text(
                    "FUNCIONES",
                    style: TextStyle(color: tab == 2 ? pcolor : Colors.white,fontFamily: 'Anton', letterSpacing: 2.0),
                  ),
                  onPressed: () {
                    setState(() {
                      tab = 2;
                    });
                  },
                  padding: EdgeInsets.all(15.0),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _tab0() {
    return SliverToBoxAdapter(
      child: Material(
        color: Color(0xFF1C2429),
        child: Column(
          children: <Widget>[
            _buildSipnosis(),
            _builCast(),
            SizedBox(
              height: 10.0,
            )
          ],
        ),
      ),
    );
  }

  Widget _tab1() {
    return new SliverToBoxAdapter(
      child: new Container(
        padding: EdgeInsets.only(top: 20.0),
        height: mheight/2.7,
        child: new Swiper(
          viewportFraction: 0.8,
          scale: 0.9,
          itemWidth: mwidth,
          itemHeight: mheight/2.7,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return new Image.network(
              mconfig.themoviedb_img+imgs[index],
              fit: BoxFit.cover,
            );
          },
          itemCount: imgs.length,
          pagination: new SwiperPagination(),

        ),
      ),
    );
  }

  Widget _tab2() {
    return SliverPadding(
      padding: EdgeInsets.only(left: 0.0),
      sliver: SliverToBoxAdapter(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: _funciones(),
        ),
      ),
    );
  }

  _funciones() {
    List<Widget> widgets = List<Widget>();
    widgets.add(_funcionColumns());
    for (var e in entradas) {
      widgets.add(_funcionRow(e));
    }
    return widgets;
  }

  Widget _funcionColumns() {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            decoration: new BoxDecoration(
                border: new Border.all(color: Color(0xFF263238))),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                "#",
                textAlign: TextAlign.center,
                style: TextStyle(fontFamily: 'Anton'),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            decoration: new BoxDecoration(
                border: new Border.all(color: Color(0xFF263238))),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                "Hora",
                textAlign: TextAlign.center,
                style: TextStyle(fontFamily: 'Anton', letterSpacing: 2.0),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            decoration: new BoxDecoration(
                border: new Border.all(color: Color(0xFF263238))),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                "Calidad",
                textAlign: TextAlign.center,
                style: TextStyle(fontFamily: 'Anton', letterSpacing: 2.0),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: Container(
            decoration: new BoxDecoration(
                border: new Border.all(color: Color(0xFF263238))),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                "Idioma",
                textAlign: TextAlign.center,
                style: TextStyle(fontFamily: 'Anton', letterSpacing: 2.0),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: Container(
            decoration: new BoxDecoration(
                border: new Border.all(color: Color(0xFF263238))),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                "Tickets",
                textAlign: TextAlign.center,
                style: TextStyle(fontFamily: 'Anton', letterSpacing: 2.0),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _funcionRow(Entrada entrada) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            decoration: new BoxDecoration(
                border: new Border.all(color: Color(0xFF263238))),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                entrada.num.toString(),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            decoration: new BoxDecoration(
                border: new Border.all(color: Color(0xFF263238))),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                entrada.hora,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            decoration: new BoxDecoration(
                border: new Border.all(color: Color(0xFF263238))),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                entrada.dimension,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: Container(
            decoration: new BoxDecoration(
                border: new Border.all(color: Color(0xFF263238))),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                entrada.idioma,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        Expanded(
            flex: 3,
            child: Container(
              decoration: new BoxDecoration(
                  border: new Border.all(color: Color(0xFF263238))),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                  entrada.disponible == true ? 'COMPRAR' : 'AGOTADO',
                  textAlign: TextAlign.center,
                ),
              ),
            ))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    this._context = context;
    mwidth = MediaQuery.of(context).size.width;
    mheight = MediaQuery.of(context).size.height;
    return new Scaffold(
        key: _scaffoldKey,
        body: movie != null
            ? CustomScrollView(
                slivers: <Widget>[
                  MovieDetailHeader(),
                  _buildHead2(),
                  _buildTabs(),
                  tab == 0 ? _tab0() : (tab == 1 ? _tab1() : _tab2())
                ],
              )
            : new Center(
                child: new CircularProgressIndicator(),
              ));
  }
}

class TriClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(size.width / 2, size.height / 3);
    path.lineTo(size.width, size.height / 1.3);
    path.lineTo(size.width, 0.0);
    path.lineTo(size.width / 2, size.height / 3);
    path.lineTo(size.width / 2, size.height / 3);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

class ArcClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(0.0, size.height - 140.0);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class Casting {
  final String name;
  final String profile_path;

  Casting({this.name, this.profile_path});

  factory Casting.fromJson(Map<String, dynamic> json) {
    return Casting(name: json['name'], profile_path: json['profile_path']);
  }
}
