import 'package:flutter/material.dart';
import 'dart:io' show Platform;
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:starcines/models/genre.dart';
import 'package:starcines/models/tvshow.dart';
import 'package:starcines/pages/home/cartelera.dart';
import 'package:starcines/pages/home/inicio.dart';
import 'package:starcines/pages/loginPage.dart';
import 'package:starcines/pages/tvPage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

//models
import 'package:starcines/models/movie.dart';
import 'package:starcines/pages/moviePage.dart';

import 'package:starcines/config.dart' as mconfig;

import 'package:firebase_messaging/firebase_messaging.dart';

final Color pcolor = Color(0xFFff1744);
final String themoviedb = mconfig.themoviedb;

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScrollController scrollController = new ScrollController();
  var isTop = true;
  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  VoidCallback _showBottomSheetCallback;

  BuildContext _scaffoldContext;

  PageController controller;
  PageController controllerPromos = new PageController();
  int currentpage = 0;
  int currentPagePromos = 0;
  bool expanded = false;

  List<Genre> genres = <Genre>[];
  DateTime selectedDate = new DateTime.now();

  var mwidth = 0.0;

  var mheight = 0.0;

  showSnackbar(text) {
    final snackBar = SnackBar(
      content: Text(text),
      duration: Duration(seconds: 4),
      backgroundColor: pcolor,
    );
    Scaffold.of(_scaffoldContext).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();

    scrollController.addListener(() {
      if (scrollController.position.pixels > 200.0) {
        setState(() {
          isTop = false;
        });
      } else {
        setState(() {
          isTop = true;
        });
      }
    });

    if (Platform.isAndroid) {
      //firebase cloud messaaging
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) {
          print("onMessage: $message");
          showSnackbar("onMessage: $message");
        },
        onLaunch: (Map<String, dynamic> message) {
          print("onLaunch: $message");
          showSnackbar("onLaunch: $message");
        },
        onResume: (Map<String, dynamic> message) {
          print("onResume: $message");
          showSnackbar("onResume: $message");
        },
      );

      //firebase permisos iOS

      //firebase get token
      _firebaseMessaging.getToken().then((String token) {
        assert(token != null);
        print("Push Messaging token: $token");
      });
    }

    controller = new PageController(
      initialPage: currentpage,
      keepPage: false,
      viewportFraction: 0.5,
    );
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  fecthGenres() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tmpGenres = prefs.getString("genres");
    final List<Genre> tmp = <Genre>[];
    if (tmpGenres == null) {
      print("null genres");
      final response = await http.get("https://api.myjson.com/bins/rcl8o");
      final parsed = JSON.decode(response.body);
      for (var item in parsed['genres']) {
        tmp.add(Genre.fromJson(item));
      }
      prefs.setString(
          "genres", JSON.encode(parsed['genres'])); //guardamos los generos

    } else {
      print("genres load");
      for (var item in JSON.decode(tmpGenres)) {
        tmp.add(Genre.fromJson(item));
      }
    }
    setState(() {
      genres = tmp;
    });
  }

  Widget _buildAppbar() {
    //Color.fromRGBO(28, 36, 41, 0.8)
    return Positioned(
      top: 0.0,
      left: 0.0,
      right: 0.0,
      child: Material(
        color: Color.fromRGBO(28, 36, 41, isTop == true ? 0.0 : 0.8),
        child: new Padding(
          padding: const EdgeInsets.only(
              left: 8.0, right: 8.0, top: 28.0, bottom: 4.0),
          child: new Row(
            children: [
              new CupertinoButton(
                  child: Image.asset(
                    'images/signin.png',
                    width: 30.0,
                  ),
                  padding: EdgeInsets.all(0.0),
                  pressedOpacity: 0.3,
                  onPressed: () {
                    //_scaffoldKey.currentState.openDrawer();
                    Navigator.push(_scaffoldContext,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  }),
              new Expanded(
                child: new Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: new Text(
                    "TOP CINEMA",
                    style: new TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w300,
                        fontFamily: 'Anton',
                        letterSpacing: 3.0),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Container(
                width: 50.0,
                child: new ListTile(
                  trailing: new PopupMenuButton(
                      padding: EdgeInsets.zero,
                      icon: new Icon(
                        Icons.more_vert,
                        color: Colors.white,
                        size: 30.0,
                      ),
                      itemBuilder: (context) => [
                            new PopupMenuItem(
                                value: "Share",
                                child: ListTile(
                                    leading: Icon(Icons.share),
                                    title: Text('Share App'))),
                            new PopupMenuItem(
                                value: "Exit",
                                child: ListTile(
                                    leading: Icon(Icons.exit_to_app),
                                    title: Text('Exit App'))),
                          ]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBottomNavigation() {
    return new Positioned(
        bottom: 0.0,
        left: 0.0,
        right: 0.0,
        child: new Material(
          color: Color.fromRGBO(28, 36, 41, 0.85),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new ImageTextButton(
                  path: 'images/dashboard.png',
                  text: "Inicio",
                  tintColor: currentpage == 0 ? Colors.amber : Colors.white,
                  onClick: () {
                    setState(() {
                      currentpage = 0;
                    });
                  }),
              new ImageTextButton(
                  path: 'images/videoplayer.png',
                  text: "Cartelera",
                  tintColor: currentpage == 1 ? Colors.amber : Colors.white,
                  onClick: () {
                    setState(() {
                      currentpage = 1;
                    });
                  }),
              new ImageTextButton(
                  path: 'images/videocamera.png',
                  text: "Proxim.",
                  tintColor: currentpage == 2 ? Colors.amber : Colors.white,
                  onClick: () {
                    setState(() {
                      currentpage = 2;
                    });
                  }),
              new ImageTextButton(
                  path: 'images/television.png',
                  text: "Series TV",
                  tintColor: currentpage == 3 ? Colors.amber : Colors.white,
                  onClick: () {
                    setState(() {
                      currentpage = 3;
                    });
                  }),
              new ImageTextButton(
                  path: 'images/ticketb.png',
                  text: "Tickets",
                  tintColor: currentpage == 4 ? Colors.amber : Colors.white,
                  onClick: () {
                    setState(() {
                      currentpage = 4;
                    });
                  })
            ],
          ),
        ));
  }

  StatefulWidget _getCurrentPage() {
    switch (currentpage) {
      case 0:
        return Inicio(
            onClick1: () {
              setState(() {
                currentpage = 1;
              });
            },
            onClick2: () {
              setState(() {
                currentpage = 2;
              });
            },
            scaffoldContext: _scaffoldContext,
            scrollController: scrollController);
        break;

      case 1:
        return Cartelera(
            scaffoldContext: _scaffoldContext,
            scrollController: scrollController);
        break;
      default:
        return Inicio(
            scaffoldContext: _scaffoldContext,
            scrollController: scrollController);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Builder(
            builder: (BuildContext context) {
              _scaffoldContext = context;
              return new Stack(children: <Widget>[
                _getCurrentPage(),
                _buildAppbar(),
                _buildBottomNavigation()
              ]);
            },
          )),
    );
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime today = new DateTime.now();
    final DateTime lastDay = today.add(Duration(days: 5));
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: new DateTime(today.year, today.month, today.day),
        lastDate: new DateTime(lastDay.year, lastDay.month, lastDay.day));

    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  getRequestToken() async {
    final res = await http.get(
        "https://api.themoviedb.org/3/authentication/token/new?api_key=$themoviedb");
    final parsed = JSON.decode(res.body);
    print(parsed);
    if (parsed['success'] == true) {
      final token = parsed['request_token'];
      final url =
          "https://www.themoviedb.org/authenticate/$token?redirect_to=cinemaecu//";
      _launchURL(url);
    }
  }
}

void toMoviePage(BuildContext mContext, Movie movie) {
  Navigator.push(mContext,
      MaterialPageRoute(builder: (context) => MoviePage(movieId: movie.id)));
}

void toTvPage(BuildContext mContext, TVShow tv) {
  Navigator.push(
      mContext, MaterialPageRoute(builder: (context) => TvPage(tvId: tv.id)));
}

String getMonth(int i) {
  switch (i) {
    case 1:
      return "Enero";
    case 2:
      return "Febrero";
    case 3:
      return "Marzo";
    case 4:
      return "Abril";
    case 5:
      return "Mayo";
    case 6:
      return "Junio";

    case 7:
      return "Julio";
    case 8:
      return "Agosto";

    case 9:
      return "Septiembre";
    case 10:
      return "Octubre";
    case 11:
      return "Noviembre";

    case 12:
      return "Diciembre";

    default:
      return "";
  }
}

class ImageTextButton extends StatelessWidget {
  final String path;
  final String text;
  var width = 30.0;
  var height = 30.0;
  final Color tintColor;

  final VoidCallback onClick;

  ImageTextButton(
      {@required this.path,
      @required this.text,
      this.width,
      this.height,
      @required this.onClick,
      @required this.tintColor});

  @override
  Widget build(BuildContext context) {
    return new CupertinoButton(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
        child: Column(
          children: <Widget>[
            Image.asset(
              path,
              height: 30.0,
              color: tintColor,
            ),
            SizedBox(
              height: 2.0,
            ),
            Text(
              text,
              style: TextStyle(color: tintColor, fontSize: 10.0),
            )
          ],
        ),
        onPressed: () {
          onClick();
        });
  }
}

enum DismissDialogAction {
  cancel,
  discard,
  save,
}
