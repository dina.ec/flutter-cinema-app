import 'dart:async';

import 'package:flutter/material.dart';
import 'package:starcines/components/MTextField.dart';
import 'package:starcines/components/PasswordField.dart';
import 'package:flutter/cupertino.dart';

final Color pcolor = Color(0xFFff1744);

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  BuildContext _context;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<FormFieldState<String>> _passwordFieldKey =
      new GlobalKey<FormFieldState<String>>();

  var email = '', nemail = '',nusername='';
  var password = '', npassword = '';
  var vpassword = '', nvpassword = '';

  bool _autovalidate = false;
  bool _formWasEdited = false;
  var tab = 0;

  _LoginPageState();

  //validamos el email ingresado
  String _validateEmail(String value) {
    _formWasEdited = true;
    return !value.contains('@') ? 'Ingrese un e-mail válido' : null;
  }

  String _validateString(String value) {
    _formWasEdited = true;
    return value.isEmpty ? 'Ingrese un texto válido' : null;
  }

  //validamos la contraseña
  String _validatePassword(String value) {
    _formWasEdited = true;
    final FormFieldState<String> passwordField = _passwordFieldKey.currentState;
    if (passwordField.value == null || passwordField.value.isEmpty)
      return 'Ingrese una contraseña válida.';
    if (passwordField.value != value) return 'Las contraseñas no coinciden';
    return null;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      backgroundColor: pcolor,
    ));
  }

  //se llama cuando se da clic en el boton ENVIAR
  void _handleSubmitted() {
    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autovalidate = true; // Start validating on every change.
      showInSnackBar(
          'Por favor corriga los errores antes de enviar el formulario.');
    }
  }

  //si se da clic en atras despues de tratar de enviar el formulario
  Future<bool> _warnUserAboutInvalidData() async {
    final FormState form = _formKey.currentState;
    if (form == null || !_formWasEdited || form.validate()) return true;

    return await showDialog<bool>(
          context: context,
          builder: (BuildContext context) {
            return new AlertDialog(
              title: const Text('Este formulario tiene errores'),
              content: const Text('¿Realmente desea dejarlo asi?'),
              actions: <Widget>[
                new FlatButton(
                  child: const Text('SI'),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                ),
                new FlatButton(
                  child: const Text('NO'),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
              ],
            );
          },
        ) ??
        false;
  }

  Widget _buildTabs() {
    return new Padding(
      padding: EdgeInsets.only(top: 25.0),
      child: Material(
        elevation: 5.0,
        color: Color(0xFF1C2429),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            new Expanded(
              child: FlatButton(
                child: Text(
                  "Iniciar Sesión",
                  style: TextStyle(
                      color: tab == 0 ? pcolor : Colors.white,
                      fontFamily: 'Anton',
                      letterSpacing: 2.0),
                ),
                onPressed: () {
                  setState(() {
                    tab = 0;
                  });
                },
                padding: EdgeInsets.all(15.0),
              ),
            ),
            new Expanded(
              child: FlatButton(
                child: Text(
                  "Nueva Cuenta",
                  style: TextStyle(
                      color: tab == 1 ? pcolor : Colors.white,
                      fontFamily: 'Anton',
                      letterSpacing: 2.0),
                ),
                onPressed: () {
                  setState(() {
                    tab = 1;
                  });
                },
                padding: EdgeInsets.all(15.0),
              ),
            )
          ],
        ),
      ),
    );
  }

  _tab1() {
    return new Material(
      color: Colors.transparent,
      child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 30.0),
          child: Form(
              key: _formKey,
              autovalidate: _autovalidate,
              child: Column(
                children: <Widget>[
                  new MTextField(
                      hintText: "E-mail *",
                      validator: _validateEmail,
                      inputType: TextInputType.emailAddress,
                      onFieldSubmitted: (String value) {
                        setState(() {
                          email = value;
                        });
                      }),
                  new SizedBox(height: 10.0),
                  new PasswordField(
                      fieldKey: _passwordFieldKey,
                      hintText: "Contraseña *",
                      onFieldSubmitted: (String value) {
                        setState(() {
                          password = value;
                        });
                      }),
                  new SizedBox(height: 15.0),
                  MaterialButton(
                    color: Colors.blue,
                    onPressed: _handleSubmitted,
                    child: SizedBox(
                      width: double.infinity,
                      child: Text(
                        'ENVIAR',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  new SizedBox(height: 30.0),
                  new Text(
                    "O inicia con:",
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                  ),
                  new SizedBox(height: 10.0),
                  new Row(
                    children: <Widget>[
                      new Material(
                        borderRadius: BorderRadius.circular(40.0),
                        child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Image.asset(
                              'images/facebook.png',
                              width: 35.0,
                            )),
                        elevation: 5.0,
                      ),
                      SizedBox(width: 20.0),
                      new Material(
                        borderRadius: BorderRadius.circular(40.0),
                        child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Image.asset(
                              'images/googleplus.png',
                              width: 35.0,
                            )),
                        elevation: 5.0,
                      )
                    ],
                    mainAxisAlignment: MainAxisAlignment.center,
                  ),
                ],
              ))),
    );
  }

  _tab2() {
    return new Material(
      color: Colors.transparent,
      child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 30.0),
          child: Form(
              key: _formKey,
              autovalidate: _autovalidate,
              onWillPop: _warnUserAboutInvalidData,
              child: Column(
                children: <Widget>[
                  new MTextField(
                      hintText: "Username *",
                      icon: Icons.person,
                      validator: _validateString,
                      inputType: TextInputType.text,
                      onFieldSubmitted: (String value) {
                        setState(() {
                          nusername = value;
                        });
                      }),
                  new SizedBox(height: 10.0),
                  new MTextField(
                      hintText: "E-mail *",
                      validator: _validateEmail,
                      inputType: TextInputType.emailAddress,
                      onFieldSubmitted: (String value) {
                        setState(() {
                          nemail = value;
                        });
                      }),
                  new SizedBox(height: 10.0),
                  new PasswordField(
                      fieldKey: _passwordFieldKey,
                      hintText: "Contraseña *",
                      onFieldSubmitted: (String value) {
                        setState(() {
                          npassword = value;
                        });
                      }),
                  new SizedBox(height: 10.0),
                  new PasswordField(
                    hintText: "V. Contraseña *",
                    onFieldSubmitted: (String value) {
                      setState(() {
                        nvpassword = value;
                      });
                    },
                    validator: _validatePassword,
                  ),
                  const SizedBox(height: 24.0),
                  MaterialButton(
                    color: Colors.blue,
                    onPressed: _handleSubmitted,
                    child: SizedBox(
                      width: double.infinity,
                      child: Text(
                        'ENVIAR',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ))),
    );
  }

  @override
  Widget build(BuildContext context) {
    this._context = context;
    return new Scaffold(
        key: _scaffoldKey,
        body: SingleChildScrollView(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Stack(
                children: <Widget>[
                  Image.asset(
                    "images/cover.jpg",
                    fit: BoxFit.fitHeight,
                    height: 340.0,
                    colorBlendMode: BlendMode.srcOver,
                    color: new Color.fromARGB(120, 20, 10, 40),
                  ),
                  new Positioned(
                      bottom: -2.0,
                      left: 0.0,
                      right: 0.0,
                      child: new Container(
                        height: 180.0,
                        decoration: new BoxDecoration(
                          gradient: new LinearGradient(
                            // new
                            // Where the linear gradient begins and ends
                            begin: Alignment.bottomCenter, // new
                            end: Alignment.topCenter, // new
                            // Add one stop for each color.
                            // Stops should increase
                            // from 0 to 1
                            stops: [0.1, 0.5, 0.7, 0.9],
                            colors: [
                              // Colors are easy thanks to Flutter's
                              // Colors class.
                              Color.fromRGBO(28, 36, 41, 1.0),
                              Color.fromRGBO(28, 36, 41, 0.6),
                              Color.fromRGBO(28, 36, 41, 0.3),
                              Color.fromRGBO(28, 36, 41, 0.0),
                            ],
                          ),
                        ),
                      )),
                  new Positioned(
                    top: 140.0,
                    left: 0.0,
                    right: 0.0,
                    child: new Center(
                        child: Text(
                      "TOP CINEMA",
                      style: TextStyle(
                          fontFamily: 'Anton',
                          color: Colors.white,
                          fontSize: 40.0,
                          letterSpacing: 2.0,
                          decoration: TextDecoration.none),
                    )),
                  ),
                  new Positioned(
                      top: 24.0,
                      left: 0.0,
                      child: Material(
                        color: Colors.transparent,
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            size: 30.0,
                          ),
                          onPressed: () {
                            Navigator.pop(_context);
                          },
                        ),
                      ))
                ],
              ),
              _buildTabs(),
              tab == 0 ? _tab1() : _tab2()
            ],
          ),
        ));
  }
}
