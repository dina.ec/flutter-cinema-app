class Season {
  final int id;
  final String air_date;
  final int episode_count;
  final String name;
  final String overview;
  final String poster_path;
  final int season_number;

  Season(
      {this.id,
      this.air_date,
      this.episode_count,
      this.name,
      this.overview,
      this.poster_path,
      this.season_number});


  factory Season.fromJSON(Map<String,dynamic> json){
    return Season(
      id: json['id'],
      air_date: json['air_date'],
      episode_count: json['episode_count'],
      name: json['name'],
      overview: json['overview'],
      poster_path: json['poster_path'],
      season_number: json['season_number'],
    );
  }


  static getSeasons(data){
     List<Season> tmp=<Season>[];
      for(var item in data){
        tmp.add(Season.fromJSON(item));
      }
     return tmp;
  }


}
