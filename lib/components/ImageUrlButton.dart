import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:starcines/models/movie.dart';
import 'package:starcines/pages/moviePage.dart';

/**
 * clase para generar un image button desde una url
 */
class ImageUrlButton extends StatelessWidget {
  final String url;
  var width = 120.0;
  var height = 180.0;

  final VoidCallback onClick;

  ImageUrlButton(
      {@required this.url, this.width, this.height, @required this.onClick});

  @override
  Widget build(BuildContext context) {
    return new CupertinoButton(
        pressedOpacity: 0.3,
        padding: EdgeInsets.all(0.0),
        child: new Container(
          width: this.width,
          height: this.height,
          margin: EdgeInsets.only(left: 2.0, right: 2.0, top: 1.0),
          child: new CachedNetworkImage(
            imageUrl: url,
            placeholder: new Center(
              child: Padding(
                  padding: EdgeInsets.all(20.0),
                  child: SpinKitPulse(
                    color: Colors.white,
                    size: 60.0,
                  ),
              ),
            ),
            errorWidget: new Icon(Icons.error),
            fit: BoxFit.cover,
          ),
        ),
        onPressed: () {
          onClick();
        });
  }
}



void toMoviePage(BuildContext mContext, Movie movie) {
  Navigator.push(mContext,
      MaterialPageRoute(builder: (context) => MoviePage(movieId: movie.id)));
}